package hometask;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Slf4j
public class StepDefs {
    @Given("I Log in to {string} page")
    public void iLogInToPage(String url) {
        Configuration.reportsFolder = "target/surefire-reports";
        open(url);
    }

    @When("I open Careers menu")
    public void iOpenCareersMenu() {
        $(byId("menu-item-127")).shouldHave(Condition.text("CAREERS")).hover();
    }

    @And("I Click Vacancies from the list")
    public void iClickVacanciesFromTheList() {
        $(byId("menu-item-131")).shouldHave(Condition.text("Vacancies")).click();
    }

    @And("I open vacancie with title {string}")
    public void iOpenVacancieWithTitle(String titleText) {
        $(byId("menu-item-3249")).shouldHave(Condition.text(titleText)).click();
    }

    @Then("I verify that paragraph Professional skills and qualification has {int} skills")
    public void iVerifyThatParagraphProfessionalSkillsAndQualificationHasSkills(int expectedResult) {
        String actualResult = $(byXpath("//div[@class='text-block']/h1[contains(text(),'Test Automation Engineer')]/following-sibling::div/p[3]"))
                .shouldHave(Condition.text("Experience")).text();
        Integer resultSize = actualResult.split("\n").length;
        assertThat(expectedResult, equalTo(resultSize));
        log.debug("Log Debug: {}", actualResult);
    }
}
