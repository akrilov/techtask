package hometask;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.junit.TextReport;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(strict = true,
                features = {"classpath:/features"},
                plugin = {"pretty", "html:target/cucumber", "json:target/cucumber/cucumber.json"}
                )
public class RunTest {
    @Rule
    public TestRule report = new TextReport().onFailedTest(true).onSucceededTest(true);

    @BeforeClass
    public static void setUp() {
        Configuration.reportsFolder = "target/surefire-reports";
    }
}
